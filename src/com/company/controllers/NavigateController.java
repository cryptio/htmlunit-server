// Copyright 2019 cpke

package com.company.controllers;

import com.company.app.Data;
import com.company.app.Request;
import com.company.app.Response;
import com.company.util.Utils;
import com.gargoylesoftware.htmlunit.*;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

@Path("/navigate")
public class NavigateController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUrl(@QueryParam("url") URL url) throws IOException {
        if (url == null) {
            throw new WebApplicationException("The parameter 'url' is required");
        }

        webPage_ = webClient_.getPage(url);
        // If the page throws a 500 we should not raise an exception, just pass on the response
        webClient_.setThrowExceptionOnFailingStatusCode(false);

        return processControllerResponse();
    }

    /**
     * Take the current web page after an HTMLUnit operation, retrieve the required data,
     * and prepare a suitable JSON response
     *
     * @return A JSON string to use for the controller's response
     */
    private String processControllerResponse() {
        final WebResponse webResponse = webPage_.getWebResponse();

        final String webResponseBody = new String(webResponse.getResponseBody());

        final Response controllerResponse = new Response(
                webResponse.getResponseHeaders(),
                webResponse.getRequestMethod().getName(),
                webResponse.getUrl(),
                webResponseBody
        );

        final Request controllerRequest = new Request(request_);

        final Data responseObject = new Data(controllerRequest, controllerResponse);

        return Utils.objectToJSON(responseObject);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String postUrl(@QueryParam("url") URL url) throws IOException {
        // Get the POST parameters
        final Map<String, String[]> postParameters = request_.getParameterMap();

        WebRequestSettings requestSettings = new WebRequestSettings(url, SubmitMethod.POST);

        // Convert the map of POST parameters into a JSON string to use in HTMLUnit
        ObjectMapper objectMapper = new ObjectMapper();
        String postJson = objectMapper.writeValueAsString(postParameters);

        requestSettings.setRequestBody(postJson);

        webPage_ = webClient_.getPage(requestSettings);

        return processControllerResponse();
    }

    @Context
    private org.glassfish.grizzly.http.server.Request request_;

    private WebClient webClient_ = new WebClient(BrowserVersion.FIREFOX_2);
    private Page webPage_;
}
