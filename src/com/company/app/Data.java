// Copyright 2019 cpke

package com.company.app;

/**
 * Data is just a wrapper class to hold the data we want to return in our controllers so the response looks like this
 * when we turn it into a JSON string:
 * {
 * "data": {
 * ...
 * }
 * }
 */
public class Data {
    public Data(Request request, Response response) {
        request_ = request;
        response_ = response;
    }

    /**
     * Return the request
     *
     * @return The stored request
     */
    public Request getRequest() {
        return request_;
    }

    /**
     * Obtain the response
     *
     * @return The response
     */
    public Response getResponse() {
        return response_;
    }

    private Request request_;
    private Response response_;
}
