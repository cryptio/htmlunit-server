// Copyright 2019 cpke

package com.company.app;

import org.apache.commons.httpclient.NameValuePair;

import java.net.URL;
import java.util.*;

public class Response {
    public Response(List<org.apache.commons.httpclient.NameValuePair> responseHeaders, String method, URL url, String responseBody) {
        method_ = method;
        url_ = url;
        headers_ = responseHeaders;
        body_ = responseBody;
    }

    /**
     * Retrieve the response headers
     *
     * @return The headers from the response
     */
    public List<NameValuePair> getHeaders() {
        return headers_;
    }

    /**
     * Retrieve the response method
     *
     * @return The response's HTTP method used
     */
    public String getMethod() {
        return method_;
    }

    public URL getURL() {
        return url_;
    }

    /**
     * Get the response body
     *
     * @return The response body as a string
     */
    public String getBody() {
        return body_;
    }

    private List<NameValuePair> headers_;
    private String method_;
    private URL url_;
    private String body_;
}
