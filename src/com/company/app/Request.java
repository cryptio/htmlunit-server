// Copyright 2019 cpke

package com.company.app;

import org.apache.commons.httpclient.NameValuePair;

import org.glassfish.grizzly.http.Cookie;
import java.util.*;

public class Request {
    public Request(org.glassfish.grizzly.http.server.Request grizzlyRequest) {
        cookies_ = grizzlyRequest.getCookies();

        /* HttpServletRequest only lets you get an Enumeration of the header names and values separately,
         * so we have to iterate over the header names and get the value for each one
         */
        final Iterable<String> headerNames = grizzlyRequest.getHeaderNames();
        for (String headerName : headerNames) {
            final NameValuePair pair = new NameValuePair(headerName, grizzlyRequest.getHeader(headerName));
            headers_.add(pair);
        }

        /* Grizzly's Request returns its method in uppercase, but we will convert it to lowercase so it is consistent
         * with Response
         */
        method_ = grizzlyRequest.getMethod().getMethodString().toLowerCase();
        requestURI_ = grizzlyRequest.getRequestURI();
        queryString_ = grizzlyRequest.getQueryString();
    }

    /**
     * Get the request cookies
     *
     * @return The request's cookies
     */
    public Cookie[] getCookies() {
        return cookies_;
    }

    /**
     * Fetch the request headers
     *
     * @return The request headers
     */
    public List<NameValuePair> getHeaders() {
        return headers_;
    }

    /**
     * Get the HTTP request method
     *
     * @return The method name of the HTTP request as a string representation
     */
    public String getMethod() {
        return method_;
    }

    /**
     * Get the query string of the request
     *
     * @return The query string used
     */
    public String getQueryString() {
        return queryString_;
    }

    /**
     * Retrieve the request URI
     *
     * @return The request URI of the request
     */
    public String getRequestURI() {
        return requestURI_;
    }

    private Cookie[] cookies_;
    List<NameValuePair> headers_ = new ArrayList<NameValuePair>();
    private String method_;
    private String queryString_;
    private String requestURI_;
}
