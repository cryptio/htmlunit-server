// Copyright 2019 cpke

package com.company.app;

public class Error {
    public Error(int status, String title, String detail) {
        status_ = status;
        title_ = title;
        detail_ = detail;
    }

    /**
     * Retrieve the error status code
     *
     * @return The status as an integer, should correspond to an HTTP status
     */
    public int getStatus() {
        return status_;
    }

    /**
     * Get the error title
     *
     * @return The title of the error
     */
    public String getTitle() {
        return title_;
    }

    /**
     * Get the details about the error
     *
     * @return More detailed information about the error
     */
    public String getDetail() {
        return detail_;
    }

    private int status_;
    private String title_;
    private String detail_;
}
