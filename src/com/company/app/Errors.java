// Copyright 2019 cpke

package com.company.app;

import java.util.List;

/**
 * Errors is just a wrapper class to hold the errors we want to return in our controllers so the response looks like this
 * when we turn it into a JSON string:
 * {
 * "errors": {
 * ...
 * }
 * }
 */
public class Errors {
    public Errors(List<Error> errors) {
        errors_ = errors;
    }

    /**
     * Get all of the errors
     *
     * @return The list of errors
     */
    public List<Error> getErrors() {
        return errors_;
    }

    private List<Error> errors_;
}
