package com.company.exceptions;

import com.company.app.Errors;
import com.company.util.Utils;
import com.company.app.Error;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

/**
 * FailingHttpStatusExceptionMapper handles the exception raised by HtmlUnit when a non-successful response code is
 * returned from the server
 */
@Provider
public class FailingHttpStatusCodeExceptionMapper implements ExceptionMapper<FailingHttpStatusCodeException> {

    public Response toResponse(FailingHttpStatusCodeException exception) {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(exception.getStatusCode(), exception.getMessage(), new String(exception.getResponse().getResponseBody())));
        final Errors errorsObj = new Errors(errors);
        final String responseJson = Utils.objectToJSON(errorsObj);

        return Response.status(Response.Status.NOT_FOUND).type(MediaType.APPLICATION_JSON_TYPE).entity(responseJson).build();
    }
}