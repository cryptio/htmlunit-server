package com.company.exceptions;

import com.company.app.Errors;
import com.company.util.Utils;
import com.company.app.Error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

/**
 * GenericExceptionMapper catches all exceptions that do not have specific exception mappers registered for them
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

    public Response toResponse(Throwable exception) {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(500, "Unexpected Error", exception.getMessage()));
        final Errors errorsObj = new Errors(errors);
        final String responseJson = Utils.objectToJSON(errorsObj);

        return Response.status(Response.Status.NOT_FOUND).type(MediaType.APPLICATION_JSON_TYPE).entity(responseJson).build();
    }
}