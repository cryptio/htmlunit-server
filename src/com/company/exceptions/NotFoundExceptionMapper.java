// Copyright 2019 cpke

package com.company.exceptions;

import com.company.app.Errors;
import com.company.util.Utils;
import com.company.app.Error;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    public Response toResponse(NotFoundException exception) {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(404, "Not Found", exception.getMessage()));
        final Errors errorsObj = new Errors(errors);
        final String responseJson = Utils.objectToJSON(errorsObj);

        return Response.status(Response.Status.NOT_FOUND).type(MediaType.APPLICATION_JSON_TYPE).entity(responseJson).build();
    }
}