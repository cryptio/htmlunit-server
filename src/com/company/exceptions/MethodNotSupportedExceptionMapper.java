// Copyright 2019 cpke

package com.company.exceptions;

import com.company.app.Errors;
import com.company.util.Utils;
import com.company.app.Error;
import org.apache.http.MethodNotSupportedException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class MethodNotSupportedExceptionMapper implements ExceptionMapper<MethodNotSupportedException> {

    public Response toResponse(MethodNotSupportedException exception) {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(405, "Method Not Allowed", exception.getMessage()));
        final Errors errorsObj = new Errors(errors);
        final String responseJson = Utils.objectToJSON(errorsObj);

        return Response.status(Response.Status.NOT_FOUND).type(MediaType.APPLICATION_JSON_TYPE).entity(responseJson).build();
    }
}