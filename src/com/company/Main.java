// Copyright 2019 cpke

package com.company;

import com.company.controllers.NavigateController;
import com.company.exceptions.FailingHttpStatusCodeExceptionMapper;
import com.company.exceptions.GenericExceptionMapper;
import com.company.exceptions.NotFoundExceptionMapper;
import com.company.exceptions.WebApplicationExceptionMapper;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ext.RuntimeDelegate;
import java.net.URI;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger l = Logger.getLogger("org.glassfish.grizzly.http.server.HttpHandler");
        l.setLevel(Level.FINE);
        l.setUseParentHandlers(false);
        ConsoleHandler ch = new ConsoleHandler();
        ch.setLevel(Level.ALL);
        l.addHandler(ch);

        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(NavigateController.class);
        resourceConfig.register(NotFoundExceptionMapper.class);
        resourceConfig.register(WebApplicationExceptionMapper.class);
        resourceConfig.register(FailingHttpStatusCodeExceptionMapper.class);
        resourceConfig.register(GenericExceptionMapper.class);

        HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(resourceConfig, HttpHandler.class);

        URI uri = null;
        try {
            uri = new URI("http://localhost:8080");
        } catch (Exception e) {
            System.err.printf("Error setting server URI: %s\n", e.getMessage());
        }

        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri, resourceConfig, false);
        server.getServerConfiguration().addHttpHandler(handler);

        try {
            server.start();
            System.out.println("Press any key to stop the server...");
            System.in.read();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
