// Copyright 2019 cpke

package com.company.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.IOException;

public class Utils {
    /**
     * objectToJSON attempts to convert an arbitrary object into a JSON string. If it does not succeed,
     * it will set a member variable to the encountered exception
     *
     * @param object The object to turn into a JSON string
     * @return The object's data in a JSON formatted string
     */
    public static String objectToJSON(Object object) {
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            return objectWriter.writeValueAsString(object);
        } catch (IOException e) {
            EXCEPTION = e;
            return null;
        }
    }

    public static Exception getException() {
        return EXCEPTION;
    }

    private static Exception EXCEPTION;
}
